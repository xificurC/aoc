(ns aoc.y20.p01
  (:require [clojure.java.io :as io] [clojure.string :as s]))
(defn test-input [] [{:policy [\a [1 3]] :pass "abcde"}
                     {:policy [\b [1 3]] :pass "cdefg"}
                     {:policy [\c [2 9]] :pass "ccccccccc"}])
(defn in-range? [v [lo hi]] (and v (<= lo v hi)))
(defn valid? [{[c r] :policy pa :pass}] (-> pa frequencies (get c) (in-range? r)))
(defn parse-range [rs] (mapv #(Integer/parseInt %) (s/split rs #"-")))
(defn parse [s] (let [[rs cs pa] (s/split s #" ")] {:policy [(first cs) (parse-range rs)] :pass pa}))
(defn input [] (eduction (map parse) (line-seq (io/reader "src/aoc/y20/p02.txt"))))
(defn valid2? [{[c r] :policy pa :pass}]
  (let [[c1 c2 :as cs] (mapv #(nth pa (dec %)) r)] (and (not= c1 c2) (some #{c} cs))))
(comment
  (->> (input) (filter valid?) count)
  (->> (input) (filter valid2?) count)
  )
