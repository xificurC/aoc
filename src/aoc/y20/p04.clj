(ns aoc.y20.p04 (:require [clojure.string :as s] [clojure.set :as set]))
(def bare-test-input
  "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in")
(defn entries [s] (s/split s #"\n\n"))
(defn entry [s] (->> (s/split s #"\s+") (map #(s/split % #":")) (into {})))
(defn valid? [mp] (-> mp keys set (set/superset? #{"byr" "iyr" "eyr" "hgt" "hcl" "ecl" "pid"})))
(defn input [] (slurp "src/aoc/y20/p04.txt"))
(defn l [s] (try (Long/parseLong s) (catch Exception _)))
(defn h? [s] (let [h (l (subs s 0 (- (count s) 2)))] (and h (if (s/ends-with? s "cm") (<= 150 h 193) (<= 59 h 76)))))
(defn valid2? [mp] (and (valid? mp),(<= 1920 (l (mp "byr")) 2002),(<= 2010 (l (mp "iyr")) 2020)
                        (<= 2020 (l (mp "eyr")) 2030),(h? (mp "hgt")),(re-matches #"#[0-9a-f]{6}" (mp "hcl"))
                        (#{"amb" "blu" "brn" "gry" "grn" "hzl" "oth"} (mp "ecl"))
                        (re-matches #"\d{9}" (mp "pid")),true))
(comment
  (->> (entries (input)) (map entry) (map valid?) frequencies)
  (->> (entries (input)) (map entry) (map valid2?) frequencies)
  )
