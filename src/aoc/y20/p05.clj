(ns aoc.y20.p05 (:require [clojure.java.io :as io]))
(defn test-input [] ["BFFFBBFRRR" "FFFBBBFRRR"])
(def decode {\F 0 \B 1 \L 0 \R 1})
(defn n [s] (Long/parseLong (apply str (map decode s)) 2))
(defn input [] (vec (line-seq (io/reader "src/aoc/y20/p05.txt"))))
(comment
  (reduce max (map n (input)))
  (filter (fn [[a b]] (= 2 (- b a))) (partition 2 1 (map n (sort (input)))))
  )
