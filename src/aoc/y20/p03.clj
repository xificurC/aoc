(ns aoc.y20.p03 (:require [clojure.java.io :as io]))
(defn build [in] {:rows (count in) :cols (count (first in)) :map in})
(defn test-input []
  (build ["..##......."
          "#...#...#.."
          ".#....#..#."
          "..#.#...#.#"
          ".#...##..#."
          "..#.##....."
          ".#.#.#....#"
          ".#........#"
          "#.##...#..."
          "#...##....#"
          ".#..#...#.#"]))
(defn at [m r c] (get-in m [:map r c])) (defn row [{rs :rows} r] (mod r rs)) (defn col [{cs :cols} c] (mod c cs))
(defn input [] (build (vec (line-seq (io/reader "src/aoc/y20/p03.txt")))))
(defn trees [m ri do] ; map right down
  (-> (reduce (fn [{r :row c :col ac :seen} _]
                (let [rr (row m (+ r do)) cc (col m (+ c ri))] {:row rr :col cc :seen (conj ac (at m rr cc))}))
              {:row 0 :col 0 :seen []}
              (range 1 (:rows m) do))
      :seen frequencies (get \#)))
(defn solve1 [] (trees (input) 3 1))
(defn solve2 [] (let [m (input)] (* (trees m 1 1) (trees m 3 1) (trees m 5 1) (trees m 7 1) (trees m 1 2))))
