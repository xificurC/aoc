(ns aoc.y20.p06 (:require [clojure.string :as s] [clojure.set :as set]))
(defn test-input [] [["abc"] ["a" "b" "c"] ["ab" "ac"] ["a" "a" "a" "a"] ["b"]])
(defn input [] (->> (s/split (slurp "src/aoc/y20/p06.txt") #"\n\n") (mapv #(s/replace % #"\s+" ""))))
(defn input2 [] (->> (s/split (slurp "src/aoc/y20/p06.txt") #"\n\n") (mapv #(mapv set (s/split % #"\n")))))
(comment
  (reduce + (map (comp count keys frequencies) (input)))
  (transduce (map count) + (map #(reduce set/intersection (set "abcdefghijklmnopqrstuvwxyz") %) (input2)))
  )
