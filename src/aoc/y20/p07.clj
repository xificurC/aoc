(ns aoc.y20.p07 (:require [clojure.string :as s] [clojure.set :as set] [clojure.java.io :as io]))
(defn test-line [] "dark orange bags contain 3 bright white bags, 4 muted yellow bags.")
(defn in [wds bg] (zipmap (->> wds (partition 4) (map #(take 2 (rest %)))) (repeat #{bg})))
(defn parseln [ln] (let [wds (s/split ln #" ")] (in (drop 4 wds) (take 2 wds))))
(defn input [] (transduce (comp (remove #(s/includes? % "no other bags")) (map parseln))
                          (completing (fn [ac nx] (merge-with set/union ac nx)))
                          {}
                          (line-seq (io/reader "src/aoc/y20/p07.txt"))))
(defn solve [mp nxs]
  (loop [rt #{} nxs nxs]
    (if-some [[nx & nxs] nxs]
      (recur (conj rt nx) (into nxs (get mp nx)))
      rt)))
(defn bags [wds] (->> wds (partition 4) (map (fn [x] [(take 2 (rest x)) (Long/parseLong (first x))]))))
(defn parseln2 [ln] (let [wds (s/split ln #" ")] [(take 2 wds) (bags (drop 4 wds))]))
(defn input2 [] (transduce (comp (remove #(s/includes? % "no other bags")) (map parseln2))
                           merge {} (line-seq (io/reader "src/aoc/y20/p07.txt"))))
(defn solve2 [mp bg]
  (loop [rt 0 bgs (get mp bg)]
    (if-some [[[bg n] & bgs] bgs]
      (recur (+ rt n) (into bgs (map (fn [[bg2 n2]] [bg2 (* n2 n)]) (get mp bg))))
      rt)))
(comment
  (dec (count (solve (input) '[("shiny" "gold")])))
  (solve2 (input2) '("shiny" "gold"))
  )
