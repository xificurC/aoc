(ns aoc.y20.p01
  (:require [clojure.java.io :as io]))
(defn test-input [] [1721 979 366 299 675 1456])
(defn input [] (eduction (map #(Integer/parseInt %)) (line-seq (io/reader "src/aoc/y20/p01.txt"))))
(defn friend [n] (- 2020 n))
(defn solve1 [ns] (reduce (fn [st nx] (if (st nx) (reduced (* nx (friend nx))) (conj st (friend nx)))) #{} ns))
(defn add-friends [fs vs nx]
  (reduce (fn [fs v] (let [sum (+ v nx)] (if (< sum 2020) (assoc fs (friend sum) [v nx]) fs))) fs vs))
(defn solve2 [ns] (reduce (fn [[fs vs] nx]
                            (if-some [fss (get fs nx)]
                              (reduced (apply * nx fss))
                              [(add-friends fs vs nx) (conj vs nx)]))
                          [{} []] ns))
(comment
  (doseq [s [solve1 solve2]] (println (s (input))))
  )
